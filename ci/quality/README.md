```yaml
include:
  - '/ci/quality/eslint.gitlab-ci.yml'

#-----------------------------------------------------------------------------------------
# Quality
#-----------------------------------------------------------------------------------------
🔎:code:quality:eslint:
  stage: 🔎code-quality
  extends: .eslint:analyzer
  needs: ["🎯:testing"]
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID
```